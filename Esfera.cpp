#include <iostream>
#include <iomanip>
#include <cmath>
#define pi 3.14159

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setValue() {
		cin >> radius;
	}
	
	double calculate() {
		return (4.0/3.0) * pi * pow(radius, 3.0);
	}
	
	void message() {
		cout << "VOLUME = ";
		cout << setiosflags (ios::fixed) << setprecision(3) << calculate() << endl;
	}
	
private:	
	int radius;
};

int main(void) {
	Data * input = new Data();
	input->setValue();
	input->message();
	return 0;
}