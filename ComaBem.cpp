#include <iostream>
#include <iomanip>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setSpent() {
		cin >> m_spent;
		m_spent *= 1.1;
	}
	
	void message() {
		cout << setiosflags (ios::fixed) << setprecision(2) << m_spent << endl;
	}
private:
	float m_spent;	
};

int main(void) {
	Data * input = new Data();
	input->setSpent();
	input->message();
	return 0;
}