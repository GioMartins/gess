#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> cars;

class Data {
public:
	Data() {
		speed.assign(2,0);
	}
	void setValues() {
		cin >> speed[0] >> speed[1] >> distance;
	}
	int elapsedTime() {
		return ((distance * 1.0)/((speed[1] - speed[0]) * 1.0))*60;
	}
	void message() {
		cout << elapsedTime() << " minutos" << endl;
	}
private:
	int distance;
	cars speed;
};

int main(void) {
	Data* input = new Data();
	input->setValues();
	input->message();
	return 0;
}