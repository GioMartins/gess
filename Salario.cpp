#include <iostream>
#include <iomanip>
using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setValues() {
		cin >> m_number >> m_hours;
		cin >> m_salary;
	}
	
	float calculate() {
		return m_hours * m_salary;
	}
	
	void message() {
		cout << "NUMBER = " << m_number << endl << "SALARY = R$ ";
		cout << setiosflags (ios::fixed) << setprecision(2) << calculate();
	}
	
private:
	int m_number, m_hours;
	float m_salary;
};

int main(void) {
	Data* input = new Data();
	input->setValues();
	input->message();
	return 0;
}