#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> watch;

class Data {
public:
	Data() {
		time.assign(3, 0);
	}
	void setValue() {
		cin >> seconds;
		aux = seconds;
	}
	void calculate() {
		while(aux >= 3600) {
			aux -= 3600;
			++time[0];
		}
		while(aux >= 60) {
			aux -= 60;
			++time[1];
		}
		time[2] = aux;
	}
	void message() {
		cout << time[0] << ":" << time[1] << ":" << time[2] << endl;
	}
private:
	int seconds, aux;
	watch time;
};

int main(void) {
	Data* input = new Data();
	input->setValue();
	input->calculate();
	input->message();
	return 0;
}