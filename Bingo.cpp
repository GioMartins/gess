#include <iostream>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setNumber() {
		cin >> m_number;
	}
	
	int message() {
		cout << "O numero sorteado foi: " << m_number << endl;
	}
	
private:
	int m_number;	
};

int main(void) {
	Data * Input = new Data();
	Input->setNumber();
	Input->message();
	return 0;
}