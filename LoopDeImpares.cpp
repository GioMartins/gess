#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> interval;

class Data {
public:
	Data() {
		oddNumbers.assign(2,0);
	}
	
	void setInterval() {
		for(int count = 0; count < 2; ++count) {
			cin >> oddNumbers[count];
		}
	}
	
	void message() {
		for(int count = oddNumbers[0]; count <= oddNumbers[1]; ++count) {
			if((count%2) && (count)) {
				cout << count << endl;
			}
		}
	}
private:
	interval oddNumbers;
};

int main(void) {
	Data * input = new Data();
	input->setInterval();
	input->message();
	return 0;
}
