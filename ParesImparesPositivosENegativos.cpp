#include <iostream>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setValue() {
		cin >> number;
	}
	
	void message() {
		if(!number) {
			cout << "NULO" << endl;
		} else {
			if(number > 0) cout << "POSITIVO ";
			else cout << "NEGATIVO ";
			if(!(number%2)) cout << "PAR" << endl;
			else cout << "IMPAR" << endl;
		}
	}
private:
	int number;	
};

int main(void) {
	Data * input = new Data();
	input->setValue();
	input->message();
	return 0;
}