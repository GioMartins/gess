#include <iostream>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setNumbers() {
		cin >> m_num1;
		cin >> m_num2;
		cin >> m_num3;
	}
	
	int getSmallest() {
		int small;
		
		if((m_num1 <= m_num2) && (m_num1 <= m_num3)) small = m_num1;
		else if ((m_num2 <= m_num1) && (m_num2 <= m_num3)) small = m_num2;
		else small = m_num3;
		
		return small;
	}
private:
	int m_num1, m_num2, m_num3;	
};

int main(void) {
	Data * Input = new Data();
	Input->setNumbers();
	cout << Input->getSmallest() << endl;
	return 0;
}