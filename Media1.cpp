#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;
typedef vector<double> grade;

class Data {
public:
	Data() {
		notes.assign(2,0);
	}
	
	void setNotes() {
		cin >> notes[0] >> notes[1];
	}
	
	double calculateMedia() {
		return ((notes[0] * 3.5) + (notes[1] * 7.5))/11.0;
	}
	
	void message() {
		cout << "MEDIA = ";
		cout << setiosflags (ios::fixed) << setprecision(5) << calculateMedia() << endl;
	}
private:
	grade notes;
};

int main(void) {
	Data * input = new Data();
	input->setNotes();
	input->message();
	return 0;
}