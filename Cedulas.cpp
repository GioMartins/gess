#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> bankNotes;

class Data {
public:
	Data() {
		ballots.assign(7,0);
	}
	void setValue() {
		cin >> total;
		aux = total;
	}
	void countBallots() {
		while(aux >= 100) {
			aux -= 100;
			++ballots[0];
		}
		while(aux >= 50) {
			aux -= 50;
			++ballots[1];
		}
		while(aux >= 20) {
			aux -= 20;
			++ballots[2];
		}
		while(aux >= 10) {
			aux -= 10;
			++ballots[3];
		}
		while(aux >= 5) {
			aux -= 5;
			++ballots[4];
		}
		while(aux >= 2) {
			aux -= 2;
			++ballots[5];
		}
		while(aux >= 1) {
			aux -= 1;
			++ballots[6];
		}
	}
	void message() {
		cout << total << endl;
		cout << ballots[0] << " nota(s) de R$ 100,00" << endl;
		cout << ballots[1] << " nota(s) de R$ 50,00" << endl;
		cout << ballots[2] << " nota(s) de R$ 20,00" << endl;
		cout << ballots[3] << " nota(s) de R$ 10,00" << endl;
		cout << ballots[4] << " nota(s) de R$ 5,00" << endl;
		cout << ballots[5] << " nota(s) de R$ 2,00" << endl;
		cout << ballots[6] << " nota(s) de R$ 1,00" << endl;
	}
private:
	int total, aux;
	bankNotes ballots;
};

int main(void) {
	Data* input = new Data();
	input->setValue();
	input->countBallots();
	input->message();
	return 0;
}