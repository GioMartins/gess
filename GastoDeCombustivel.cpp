#include <iostream>
#include <iomanip>

using namespace std;

class Data {
public:
	Data() {
		
	}
	void setValues() {
		cin >> time >> speed;
	}
	float spent() {
		return (time * speed)/12.0;
	}
	void message() {
		cout << setiosflags(ios::fixed) << setprecision(3) << spent() << endl;
	}
	
private:
	int speed, time;	
};

int main(void) {
	Data* input = new Data();
	input->setValues();
	input->message();
	return 0;
}