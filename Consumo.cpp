#include <iostream>
#include <iomanip>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setValues() {
		cin >> distance >> fuel;
	}
	
	float calculate() {
		return (distance/fuel);
	}
	
	void message() {
		cout << setiosflags(ios::fixed) << setprecision(3) << calculate() << " km/l" << endl;
	}
	
private:
	int distance;
	float fuel;
};

int main(void) {
	Data * input = new Data();
	input->setValues();
	input->message();
	return 0;
}