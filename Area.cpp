#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#define pi 3.14159

using namespace std;
typedef vector<double> values;

class Data {
public:
	Data() {
		info.assign(3, 0.0);
	}
	
	void setValues() {
		cin >> info[0] >> info[1] >> info[2];
	}
	
	double triangleArea() {
		return ((info[0] * info[2])/2.0);
	}
	
	double circleArea() {
		return (pi * pow(info[2], 2.0));
	}
	
	double trapezeArea() {
		return (((info[0] + info[1])*info[2])/2.0);
	}
	
	double squareArea() {
		return (pow(info[1], 2.0));
	}
	
	double rectangleArea() {
		return (info[0] * info[1]);
	}
	
	void message() {
		cout << "TRIANGULO: " << setiosflags(ios::fixed) << setprecision(3) << triangleArea()  << endl;
		cout << "CIRCULO: "   << setiosflags(ios::fixed) << setprecision(3) << circleArea()    << endl;
		cout << "TRAPEZIO: "  << setiosflags(ios::fixed) << setprecision(3) << trapezeArea()   << endl;
		cout << "QUADRADO: "  << setiosflags(ios::fixed) << setprecision(3) << squareArea()    << endl;
		cout << "RETANGULO: " << setiosflags(ios::fixed) << setprecision(3) << rectangleArea() << endl;
	}
	
private:	
	values info;
};

int main (void) {
	Data * input = new Data();
	input->setValues();
	input->message();
	return 0;
}