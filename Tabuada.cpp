#include <iostream>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setNumber() {
		cin >> m_number;
	}
	
	void message() {
		for(int count = 1; count <= 9; ++count) {
			cout << m_number << " X " << count << " = " << m_number*count << endl;
		}
	}
private:
	int m_number;	
};

int main(void) {
	Data * input = new Data();
	input->setNumber();
	input->message();
	return 0;
}