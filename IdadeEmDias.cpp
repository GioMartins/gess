#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> Time;

class Data {
public:
	Data() {
		age.assign(3, 0);
	}
	void setValues() {
		cin >> days;
		aux = days;
	}
	void calculate() {
		while(aux >= 365) {
			aux -= 365;
			++age[0];
		}
		while(aux >= 30) {
			aux -= 30;
			++age[1];
		}
		age[2] = aux;
	}
	void message() {
		cout << age[0] << " ano(s)"  << endl;
		cout << age[1] << " mes(es)" << endl;
		cout << age[2] << " dia(s)"  << endl;
	}
private:
	int days, aux;
	Time age;
};

int main(void) {
	Data* input = new Data();
	input->setValues();
	input->calculate();
	input->message();
	return 0;
}