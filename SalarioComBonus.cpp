#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setValues() {
		getline(cin, name);
		cin >> salary >> sales;
	}
	
	double calculate() {
		return salary + (0.15 * sales);
	}
	
	void message() {
		cout << "TOTAL = R$ ";
		cout << setiosflags (ios::fixed) << setprecision(2) << calculate() << endl;
	}
private:
	string name;
	double salary, sales;	
};

int main(void) {
	Data * input = new Data();
	input->setValues();
	input->message();
	return 0;
}