#include <iostream>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setMonth() {
		cin >> m_month;
	}
	
	void getMonth() const {
		if((m_month < 1) || (12 < m_month)) {
			cout << "invalido" << endl;
		} else if (m_month == 1) cout << "janeiro" << endl;
		else if(m_month == 2) cout << "fevereiro" << endl;
		else if(m_month == 3) cout << "marco" << endl;
		else if(m_month == 4) cout << "abril" << endl;
		else if(m_month == 5) cout << "maio" << endl;
		else if(m_month == 6) cout << "junho" << endl;
		else if(m_month == 7) cout << "julho" << endl;
		else if(m_month == 8) cout << "agosto" << endl;
		else if(m_month == 9) cout << "setembro" << endl;
		else if(m_month == 10) cout << "outubro" << endl;
		else if(m_month == 11) cout << "novembro" << endl;
		else if(m_month == 12) cout << "dezembro" << endl;
	}
private:
	int m_month;	
};

int main(void) {
	Data * input = new Data();
	input->setMonth();
	input->getMonth();
	return 0;
}