#include <iostream>

using namespace std;

class Data {
public:
	Data() {
		
	}
	
	void setNumber() {
		cin >> m_number;
	}
	
	void output() {
		cout << m_number - 1 << " " << m_number + 1 << endl;
	}
private:
	int m_number;	
};

int main(void) {
	Data * Input = new Data();
	Input->setNumber();
	Input->output();
	return 0;
}