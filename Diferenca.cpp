#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> subtract;

class Data {
public:
	Data() {
		numbers.assign(4,0);
	}
	
	void setNumbers() {
		cin >> numbers[0] >> numbers[1] >> numbers[2] >> numbers[3];
	}
	
	int calculateResult() {
		return ((numbers[0] * numbers[1]) - (numbers[2] * numbers[3]));
	}
	
	void message() {
		cout << "DIFERENCA = ";
		cout << calculateResult() << endl;
	}
private:
	subtract numbers;
};

int main(void) {
	Data * input = new Data();
	input->setNumbers();
	input->message();
	return 0;
}