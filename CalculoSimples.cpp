#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;
typedef vector<int> info1;
typedef vector<float> info2;

class Data {
public:
	Data() {
		quantity.assign(2, 0.0);
		price.assign(2, 0.0);
	}
	
	void setValues() {
		for(int count = 0; count < 2; ++count) {
			cin >> code >> quantity[count] >> price [count];
		}
	}
	
	float calculate() {
		return ((quantity[0] * price[0]) + (quantity[1] * price[1]));
	}
	
	void message() {
		cout << "VALOR A PAGAR: R$ ";
		cout << setiosflags (ios::fixed) << setprecision(2) << calculate() << endl;
	}
	
private:
	int code;
	info1 quantity;
	info2 price;
};

int main(void) {
	Data * input = new Data();
	input->setValues();
	input->message();
	return 0;
}