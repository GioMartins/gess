#include <iostream>
#include <vector>

using namespace std;
typedef long double Ldouble;
typedef vector<Ldouble> notes;

class Data {
public:
	Data() {
		grade.assign(3,0);
	}
	
	void setGrade() {
		for(int count = 0; count < 3; ++count) {
			cin >> grade[count];
		}
	}
	
	void setMedia() {
		media = (grade[0] + grade[1] + grade[2])/3;
	}
	
	void message() {
		if(media >= 7.0) {
			cout << "aprovado" << endl;
		} else if(media >= 3) {
			cout << "prova final" << endl;
		} else {
			cout << "reprovado" << endl;
		}
	}
	
private:
	notes grade;
	Ldouble media;
};

int main(void) {
	Data * Input = new Data();
	Input->setGrade();
	Input->setMedia();
	Input->message();
	return 0;
}