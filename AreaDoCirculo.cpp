#include <iostream>
#include <iomanip>
#define pi 3.14159

using namespace std;
typedef long double Ldouble;

class Data {
public:
	Data() {
		
	}
	
	void setRadius() {
		cin >> m_radius;
		m_radius /= 100;
	}
	
	Ldouble calculateTheArea() {
		return (pi * m_radius * m_radius);
	}
	
	void message() {
		cout << "Area = ";
		cout << setiosflags (ios::fixed) << setprecision(4) << calculateTheArea() << endl;
	}
private:
	 Ldouble m_radius;
};

int main(void) {
	Data * input = new Data();
	input->setRadius();
	input->message();
	return 0;
}