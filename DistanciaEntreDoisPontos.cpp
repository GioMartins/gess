#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

using namespace std;
typedef vector<int> points; 

class Data {
public:
	Data() {
		p.assign(2,0);
		q.assign(2,0);
	}
	void setPoints() {
		cin >> p[0] >> p[1] >> q[0] >> q[1];
	}
	double distance() {
		return sqrt(pow(p[0] - q[0], 2.0) + pow(p[1] - q[1], 2.0));
	}
	void message() {
		cout << setiosflags(ios::fixed) << setprecision(4) << distance() << endl;
	}
private:
	points p, q;
};

int main(void) {
	Data* input = new Data();
	input->setPoints();
	input->message();
	return 0;
}