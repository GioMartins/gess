#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;
typedef vector<int> integers;

class Data {
public:
	Data() {
		values.assign(3,0);
	}
	
	void setValues() {
		cin >> values[0] >> values[1] >> values[2];
	}
	
	int biggerValue() {
		int bigger, aux;
		aux = (values[0] +values[1] + abs(values[0] - values[1]))/2;
		bigger = (aux + values[2] + abs(aux - values[2]))/2;
		return bigger;
	}
	
	void message() {
		cout << biggerValue() << " eh o maior" << endl;
	}
	
private:
	integers values;
};

int main(void) {
	Data * input = new Data();
	input->setValues();
	input->message();
	return 0;
}