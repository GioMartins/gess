#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;
typedef vector<double> grade;

class Data {
public:
	Data() {
		notes.assign(3,0);
	}
	
	void setNotes() {
		cin >> notes[0] >> notes[1] >> notes[2];
	}
	
	double calculateMedia() {
		return ((notes[0] * 2) + (notes[1] * 3) + (notes[2] * 5))/10;
	}
	
	void message() {
		cout << "MEDIA = ";
		cout << setiosflags (ios::fixed) << setprecision(1) << calculateMedia() << endl;
	}
private:
	grade notes;
};

int main(void) {
	Data * input = new Data();
	input->setNotes();
	input->message();
	return 0;
}