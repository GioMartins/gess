#include <iostream>
#include <vector>

using namespace std;
typedef vector<int> sum;

class Data {
public:
	Data() {
		numbers.assign(3,0);
	}
	
	void setNumbers() {
		cin >> numbers[0] >> numbers[1];
		numbers[2] = numbers[0] + numbers[1];
	}
	
	void message() {
		cout << "X = " << numbers[2] << endl;
	}
private:
	sum numbers;
};

int main(void) {
	Data * input = new Data();
	input->setNumbers();
	input->message();
	return 0;
}